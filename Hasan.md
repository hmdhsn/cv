# Ahmad Hasan Ari

![Photo](Hasan.png "Photo")

## Personal Information
* Date of Birth: 27th April 2009
* Nationality: Indonesia

## Academic History
* 2016-2021: Madrasah Irsyad Zuhri Al-Islamiah, 277 Braddell Rd, Singapore
* 2022-    : SMPIT Sahabat Al-Qur'an, Binjai, Sumatera Utara, Indonesia

## Academic Achievements
### 2017
* SMKC (Singapore Math Kangaroo Contest) 2017, Primary 2, **Silver Award**
* SASMO (Singapore and Asian Schools Math Olympiad) 2017, Primary 2, **Bronze Award**
* UNSW Global, ICAS English 2017, Primary 2, **Distinction**
### 2018
* SASMO (Singapore and Asian Schools Math Olympiad) 2018, Primary 3, **Bronze Award**
* AMO (American Math Olympiad) 2018, Primary 3, **Bronze Award**
### 2019
* RIPMWC (Raffles Institution Primary Mathematics World Contest) 2019, Round 1 Junior Category, **Merit 72.37 percentile**
* SASMO (Singapore and Asian Schools Math Olympiad) 2019, Primary 4, **Bronze Award**
* UNSW Global, ICAS Science 2019, Primary 4, **Distinction**
* UNSW Global, REACH Assessments English 2019, Primary 4, **Exemplary**
* Madrasah Irsyad Colours Award 2019, **Silver**
### 2020
* SMKC (Singapore Math Kangaroo Contest) 2020, Primary 5, **Bronze Award**
* SASMO (Singapore and Asian Schools Math Olympiad) 2020, Primary 5, **Bronze Award**
* DOKA (Depth of Knowledge Assessments) 2020, Grade 05, **Distinction**
* Madrasah Irsyad, Haflah 2020, **Best in Math Primary 5**
### 2021
* SMKC (Singapore Math Kangaroo Contest) 2021, Primary 6, **Silver Award**
* SASMO (Singapore and Asian Schools Math Olympiad) 2021, Primary 6, **Silver Award**
* NUS High, NMOS (National Mathematical Olympiad of Singapore) 2021, Primary 6, **Honourable Mention**
* Hwa Chong Institution, SMOPS (Singapore Mathematical Olympiad for Primary Schools) 2021, **Silver Award**
* VANDA Science 2021, Grade 06, **Gold Award**
* AMO (American Math Olympiad) 2021, Primary 6, **Bronze Award**
* Madrasah Irsyad, Haflah 2021, **Best in Math Primary 6**

